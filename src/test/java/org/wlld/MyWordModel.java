package org.wlld;

import java.util.List;

public class MyWordModel {
    private List<KeyWordModelMapping> keyWordModelMappings;

    public List<KeyWordModelMapping> getKeyWordModelMappings() {
        return keyWordModelMappings;
    }

    public void setKeyWordModelMappings(List<KeyWordModelMapping> keyWordModelMappings) {
        this.keyWordModelMappings = keyWordModelMappings;
    }
}
